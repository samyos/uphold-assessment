import './Button.css';
import React from 'react';

const Button = (props) => {
  const { clicked } = props
  return (
    <button onClick={clicked} className="button">
        {props.children}
    </button>
  );
};

export default Button;
