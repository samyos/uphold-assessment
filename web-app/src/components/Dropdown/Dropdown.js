import './Dropdown.css';
import React, { useState, useEffect } from 'react';
import { ReactComponent as DropdownIc } from '../../assets/dropdown-icon.svg';
import { CurrencyTypes } from '../../data/currency_types';
import useTicker from '../../hooks/useTicker';


const Dropdown = (props) => {
  const { dropdownChanged } = props;
  const [selected, setSelected] = useState('USD');
  const [open, setOpen] = useState(false);

  const [rates, search] = useTicker(selected);
  

  useEffect(() => {
    if(rates.length > 0) {
      dropdownChanged({currency: selected, rates: rates});
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [rates]);

  const handleSelectionChange = (selected) => {
    setSelected(selected);
    setOpen(!open);
    search(selected);
  }

  const getCurrencyImage = (currency) => {
    const properties = CurrencyTypes.find(x => x.text === currency);
    return properties.img;
  } 

  return (
    <div>
        <div className="dropdown_container" onClick={() => setOpen(!open)}>
          <img className="image" alt="" src={getCurrencyImage(selected)}></img>
            <div className="text">{selected}</div>
            <DropdownIc className="icon"></DropdownIc>
        </div>
        {open ? <DropdownContent selected={selected} onChange={(x) => handleSelectionChange(x)}></DropdownContent> : null}
        
    </div>
  );
};


const DropdownContent = props => {
    const { selected, onChange } = props;
    const currency_types = CurrencyTypes.filter(x => x.text !== selected); //Show all the currency types minus the selected one

    return (
        <div className="dropdown_content">
          {currency_types.map(function(x, index){
              return (
                <div className="dropdown_content_item" key={index} onClick={() => onChange(x.text)}>
                  <img className="image ml-8" alt="" src={x.img}></img>
                  <div className="text dropdown_content_last_item mr-26">{x.text}</div>
                </div>
              );
          })}
        </div>
    );
  };

export default Dropdown;
