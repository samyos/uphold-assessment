import './List.css';
import React from 'react';

const List = (props) => {
    const { listRates } = props;
  return (
    <div>
        {listRates.map(function(x, index){
              return (
                <div className="list_container" key={index}>
                    <div className="value">{x.value}</div>
                    <div className="image_container">
                        <img className="image" alt="" src={x.img}></img>
                        <div className="currency">{x.currency}</div>
                    </div>
                </div>
              );
          })}
    </div>
  );
};

export default List;
