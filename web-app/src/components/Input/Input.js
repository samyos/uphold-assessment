import './Input.css';
import React, { useState, useEffect } from 'react';
import Dropdown from '../Dropdown/Dropdown'
import mathRates from '../../common/helper'
import useDebounce from '../../hooks/useDebounce';

const Input = (props) => {
  const { listRates } = props;
  const [input, setInput] = useState('');
  const [currency, setCurrency] = useState('');
  const [rates, setRates] = useState([]);

  const debouncedSearchTerm = useDebounce(input, 300);;


  useEffect(
    () => {
      addRates(debouncedSearchTerm, currency, rates);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [debouncedSearchTerm]
  );


  
  const inputOnChange = (value) => {
    if(value === '') {
      listRates([]);
    }
    setInput(value);
    
  }

  const dropdownonChange = (selected) => {
    const { currency, rates } = selected;
    setCurrency(currency);
    setRates(rates);
    addRates(input, currency, rates);
  }

  const addRates = (input, currency, rates) => { 
    if(input !== "" && rates.length > 0)
    listRates(mathRates(rates, currency, input));
  }

  


  return (
    <div className="input_container">
      <input className="input"
        type="number"
        placeholder="0.00"
        value={input}
        onChange={e => inputOnChange(e.target.value)}
      />
      <Dropdown dropdownChanged={dropdownonChange}></Dropdown>
    </div>
  );
};

export default Input;
