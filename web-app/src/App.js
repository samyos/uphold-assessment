import './App.css';
import React, { useState } from 'react';
import { ReactComponent as Logo } from './assets/logo.svg';
import Button from './components/Button/Button';
import Input from './components/Input/Input';
import List from './components/List/List';
import { key, clienteId} from './common/constants';




function App() {
  const [list_rates, setListRates] = useState([]);
  const url = "https://sandbox.uphold.com/authorize/" + clienteId + '?scope=user:read&state=' + key; //FIXME: Add hash to key
  return (
    <div className="App">
      <div className="header">
        <div className="header_elements row_space-between">
          <p className="text pointer">Personal</p>
          <p className="text pointer">Business</p>
          <p className="text pointer">Partners</p>
        </div>
        <div className="header_elements" onClick={() => window.open('https://uphold.com/en-us/',"_self")}>
          <Logo className="pointer"></Logo>
        </div>
        <div className="header_elements">
          <Button clicked={() => window.open(url,"_self")}>Log In</Button>
        </div>
      </div>
      <div className="content">
        <h2>Currency Converter</h2>
        <p className="text">Receive competitive and transparent pricing with no hidden spreads. See how we compare.</p>
        <Input listRates={setListRates}></Input>
        <div>
        <List listRates={list_rates}></List>
      </div>
      </div>
      <div className="bottom">
        <div className="divider"></div>
        <div className="grid">
          <div className="bottom_logo_container">
            <div onClick={() => window.open('https://uphold.com/en-us/',"_self")}>
              <Logo className="bottom_logo pointer"></Logo>
            </div>
          </div>
          <div className="grid-container">
            <div className="grid-item text pointer">Consumer</div>
            <div className="grid-item text pointer">About</div>
            <div className="grid-item text pointer">FAQ & Support</div>  
            <div className="grid-item text pointer">Business</div>
            <div className="grid-item text pointer">Carrers</div>
            <div className="grid-item text pointer">Platform Status</div>  
            <div className="grid-item text pointer">Partner</div>
            <div className="grid-item text pointer">Press</div>
            <div className="grid-item text pointer">Criptionary</div>  
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
