import { CurrencyTypes } from '../data/currency_types';

const mathRates = (rates, currency_selected, input) => {
    let rates_currency = [];
    for(const element of CurrencyTypes) {
        if(currency_selected === element.text) continue;
        const rate = rates.find(x => x.currency === element.text && x.pair === currency_selected + element.text);
        const math = (input * rate.ask) / 1;
        rates_currency.push({value: math, currency: rate.currency, img: element.img})
    }
    return rates_currency;
}


export default mathRates;



