import usd from '../assets/USD.png';
import eur from '../assets/EUR.png';
import bat from '../assets/BAT.png';
import btc from '../assets/BTC.png';
import bch from '../assets/BCH.png';
import cny from '../assets/CNY.png';
import eth from '../assets/ETH.png';
import gbp from '../assets/GBP.png';
export const CurrencyTypes = [
    {
        img: usd,
        text: 'USD'
    },
    {
        img: eur,
        text: 'EUR'
    },
    {
        img: bat,
        text: 'BAT'
    },
    {
        img: btc,
        text: 'BTC'
    },
    {
        img: bch,
        text: 'BCH'
    },
    {
        img: cny,
        text: 'CNY'
    },
    {
        img: eth,
        text: 'ETH'
    },
    {
        img: gbp,
        text: 'GBP'
    },

]

