import { useState, useEffect } from 'react';
import SDK from '@uphold/uphold-sdk-javascript';
import { clienteId, clientSecret } from '../common/constants';

const sdk = new SDK({
  baseUrl: '',
  clientId: clienteId,
  clientSecret: clientSecret
});

const useTicker = (value) => {
  const [rates, setRates] = useState([]);

  useEffect(() => {
    search(value);
  }, [value]);

  const search = async (term) => {
    sdk.getTicker(term)
    .then(x => setRates(x));
  };

  return [rates, search];
};

export default useTicker;
